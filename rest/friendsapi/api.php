<?php

require_once 'vendor/autoload.php';

/*
 * site : lehu.000webhost.com
 * pass : 111111
 * 
 * 
 * dbname = id8105324_android
 * username: id8105324_android
 * ppwd: android
 */

if(true) {
DB::$dbName = 'id8105324_android';
DB::$user= 'id8105324_android';
DB::$password ="android";

//DB::$encoding ="utf8";
//DB::$host = '';
//DB::$port = 3333;

}else {
    DB::$dbName = 'friendsdb';
DB::$user= 'friendsdb';
DB::$password ="friendsdb";
DB::$encoding ="utf8";
DB::$host = '127.0.0.1';
DB::$port = 3333;
}

$app = new \Slim\Slim();



$app->response()->header('content-type','application/json');

$app->get('/friends', function(){
    $friendsList = DB::query("SELECT * FROM friends");
    echo json_encode($friendsList,JSON_PRETTY_PRINT);
});


//$app->get('/friends/:id', function($id){
//    $friendsList = DB::queryFirstRow("SELECT * FROM friends where id=%i",$id);
//    echo json_encode($friendsList,JSON_PRETTY_PRINT);
//});


$app->get('/friends/:id', function($id) use ($app) {
    $friend = DB::queryFirstRow("SELECT * FROM friends where id=%i",$id);
    if($friend){
        echo json_encode($friend,JSON_PRETTY_PRINT);
    } else {
        $app->response()->status(404);
        echo json_encode("404 not found!");
    } 
});

$app->post('/friends',function() use ($app) {
  $json = $app->request()->getBody();
  $data = json_decode($json,TRUE);
  //todo: verify friend data is valid
  DB::insert('friends',$data);
  $app->response()->status(201);  
  $id = DB::insertId();
  echo json_decode($id);
});


$app->delete('/friends/:id',function($id){
    DB::delete('friends','id=%d',$id);
    echo json_encode(DB::affectedRows()!=0);
});



$app->put('/friends/:id',function() use ($app) {
  $json = $app->request()->getBody();
  $data = json_decode($json,TRUE);
  //todo: verify friend data is valid
  DB::update('friends',$data, 'id=%d',$id);
  echo json_decode(TRUE);
});


$app->run();