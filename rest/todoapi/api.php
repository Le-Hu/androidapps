<?php

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$log = new Logger('main');
$log->pushHandler(new StreamHandler('log/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('log/error.log', Logger::ERROR));

DB::$dbName = 'todoauthdb';
DB::$user = 'todoauthdb';
DB::$password = "todoauthdb";
DB::$encoding = "utf8";
DB::$host = '127.0.0.1';
DB::$port = 3333;


DB::$error_handler = 'error_handler';
DB::$nonsql_error_handler = 'error_handler';


$app = new \Slim\Slim();
$app->response()->header('content-type', 'application/json');

function error_handler($params) {
    global $log;
    if (isset($params['query'])) {
        $log->err("SQL Error:" . $params['error']);
        $log->err("SQL Query:" . $params['query']);
    } else {
        $log->err("Database error:" . $params['query']);
    }

    http_response_code(500);
    header('content-type:application/json');
    echo json_encode('500 internal erro');

    die; // don't want to keep going if a query broke
}

function getAuthUser() {
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        return FALSE;
    }

    $email = $_SERVER['PHP_AUTH_USER'];
    $pass = $_SERVER['PHP_AUTH_PW'];
    $authUser = DB::queryFirstRow("select * from users where email =s%", $email);
    if (!$authuser) {
        return FALSE;
    }
    if ($authUser['password'] == $pass) {
        return $authUser;
    } else {
        return FALSE;
    }
}

/**
 * 
 * @param type $user
 */
function isUserValid($user) {
    if (is_null($user))
        return "json parsing failed, user is null";
    if (count($user) != 2)
        return "Invalid number of valuse received";
    if (filter_var($user['email'], FILTER_VALIDATE_EMAIL) === FALSE)
        return "email is invalid";
    if (strlen($user['password']) < 8)
        return "password too short, must to be 8 minimum";

    return TRUE;
}

function isTodoValid($todo) {
    if (is_null($todo))
        return "todo is null";
    if (count($todo) != 3)
        return "Invalid number of valuse";
    if (strlen($todo['task']) < 1 || str_getcsv($todo['task'] > 100))
        return "task is too short or too long";
    if (date_create_from_format("Y-m-j", $todo['dueDate']) === FALSE)
        return "due date is invlid";
    if (!in_array($todo['isDone'], array('pending', 'done')))
        return "is done invalid, must be pending or done";
}

$app->notFound(function() use ($app) {
    $app->response()->status(404);
    echo json_encode("404 - not found");
});

/**
 * post /users
 */
$app->post('/users', function() use ($app) { // NO AUTHENTICATION REQUIRED
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify user data is valid
    $result = isUserValid($data);
    if ($result !== TRUE) {
        echo json_encode("400 - " . $result);
        $app->response()->status(400);
        return;
    }
    // make sure email is not already in use
    $isEmailInUse = DB::queryFirstField("SELECT COUNT(*) FROM users WHERE email=%s", $data['email']);
    if ($isEmailInUse) {
        echo json_encode("400 - email already in use");
        $app->response()->status(400);
        return;
    }
    DB::insert('users', $data);
    $app->response()->status(201);
    $id = DB::insertId();
    echo json_encode($id);
});


$app->get('/todos', function() {
    $todosList = DB::query("SELECT * FROM todos");
    echo json_encode($todosList, JSON_PRETTY_PRINT);
});


$app->put('/users/:email', function($email) {
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    //todo: verify friend data is valid
    DB::update('todos', $data, 'email=%s', $email);
    echo json_decode(TRUE);
});

//--------
$app->get('/users/:email', function($email) use ($app) {
    $user = DB::queryFirstRow("SELECT * FROM todos where email=%s", $email);
    if ($user) {
        echo json_encode($user, JSON_PRETTY_PRINT);
    } else {
        $app->response()->status(404);
        echo json_encode("404 not found!");
    }
});



$app->delete('/todos/:id', function($id) {
    DB::delete('todos', 'id=%d', $id);
    echo json_encode(DB::affectedRows() != 0);
});

$app->run();
