package com.lehu.quiz3;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {Trip.class}, version = 2, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public static final String DB_Name = "Trip_DB";

    public abstract DaoAccess daoAccess();


    public static AppDatabase getInstance(Context context) {
        return Room.databaseBuilder(context,
                AppDatabase.class, AppDatabase.DB_Name).fallbackToDestructiveMigration().build();
    }

}
