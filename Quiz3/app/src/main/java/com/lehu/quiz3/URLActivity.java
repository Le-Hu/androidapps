package com.lehu.quiz3;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class URLActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.toString();

//    DownloadAsyncTask downloadAsyncTask = null;

    EditText editTextURL;
    Button btnStartCancel;
    ProgressBar pbDownloadProgress;
    ImageView imageView;


    byte[] imgResult;


    SharedPreferences sharedPreferences;
    public final static String PREF_URL = "url";
    DownloaderAsyncTask downloaderAsyncTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_url);

        editTextURL = (EditText) findViewById(R.id.etURL);
        btnStartCancel = (Button) findViewById(R.id.btnStartCancel);
        pbDownloadProgress = (ProgressBar) findViewById(R.id.progressBar);

        imageView = (ImageView) findViewById(R.id.imageViewURL);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.save_to_gallery, menu);
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_save_to_gallery:
                Log.d(TAG, "onOptionsItemSelected: save file to gallery");
                saveFileToGallery();
                finish();
                break;

        }
        return true;
    }

    /**
     * save image to file
     */
    private void saveFileToGallery() {
        //            Toast.makeText(URLActivity.this, "Image saved", Toast.LENGTH_LONG).show();

        Log.d(TAG, "saveFileToGallery: save to gallery imgresult length " + imgResult.length);
        Bitmap bmp = BitmapFactory.decodeByteArray(imgResult, 0, imgResult.length);
        MediaStore.Images.Media.insertImage(getContentResolver(), bmp, "title", "desc");
        Toast.makeText(URLActivity.this, "Image saved", Toast.LENGTH_LONG).show();


    }


    public void onClickStartCanel(View view) {
        // download async task from intenent;

        if (downloaderAsyncTask == null) { // start
            try {
                String urlStr = editTextURL.getText().toString();
                URL url = new URL(urlStr); // may fail with exception
                downloaderAsyncTask = new DownloaderAsyncTask();
                downloaderAsyncTask.execute(url);
            } catch (MalformedURLException ex) {
                Toast.makeText(this, "URL invalid", Toast.LENGTH_LONG).show();
            }
        } else { // cancel
            downloaderAsyncTask.cancel(true);
        }


    }


    class DownloaderAsyncTask extends AsyncTask<URL, Integer, byte[]> {

        public final static String TAG = "DownloaderAsyncTask";

        @Override
        protected byte[] doInBackground(URL... urlArray) {
            URL url = urlArray[0];
            BufferedInputStream bufferedInputStream = null;
            // StringBuilder result = new StringBuilder();
            ByteArrayOutputStream result = new ByteArrayOutputStream();
            try {
                byte dataBuffer[] = new byte[1024];
                int totalBytesRead = 0, contentSize, bytesRead;

                URLConnection conn = url.openConnection();
                contentSize = conn.getContentLength(); // FIXME: handle -1 returned here
                String contentType = conn.getContentType();
                Log.d(TAG, "Content type is: " + contentType);
                Log.d(TAG, "Content size is: " + contentSize);

                bufferedInputStream = new BufferedInputStream(conn.getInputStream());

                while ((bytesRead = bufferedInputStream.read(dataBuffer)) != -1) {
                    if (isCancelled()) {
                        break;
                    }
                    //result.append(new String(dataBuffer));
                    result.write(dataBuffer, 0, bytesRead);
                    //
                    totalBytesRead += bytesRead;
                    if (contentSize != -1) {
                        int progressPercentage = 100 * totalBytesRead / contentSize;
                        publishProgress(progressPercentage);
                        // FIXME: remove in the real app, delay for demonstration purposes only
                        try {
                            int delay = 4000 / (contentSize / 1024);
                            Thread.sleep(delay);
                        } catch (InterruptedException ex) {
                        }
                    } else {
                        publishProgress(totalBytesRead % 100);
                    }
                }
            } catch (IOException ex) {
                Log.e(TAG, "Exception while reading from URL" + url, ex);
                return null;
            } finally {
                if (bufferedInputStream != null) {
                    try {
                        bufferedInputStream.close();
                    } catch (IOException ex) {
                        Log.e(TAG, "Exception while closing BufferedInputStream", ex);
                    }
                }
            }

            return result.toByteArray();
        }

        @Override
        public void onPreExecute() {
            // save URL from EditText to preferences
            String url = editTextURL.getText().toString();
            //sharedPreferences.edit().putString(PREF_URL, url).commit();
            // change button to "cancel download"
            btnStartCancel.setText("Cancel Download");
        }

        @Override
        public void onProgressUpdate(Integer... progValuesArray) {
            int progress = progValuesArray[0];
            Log.d(TAG, "Progress: " + progress);
            // set progress bar to show progress
            pbDownloadProgress.setProgress(progress);
        }

        @Override
        public void onCancelled() {
            Log.d(TAG, "Task cancelled");
            // change button back to "start download"
            btnStartCancel.setText("Start Download");
            // set progress bar to 0
            pbDownloadProgress.setProgress(0);
            // set downloaderAsyncTask back to null
            downloaderAsyncTask = null;
            // cleanup the view
            //tvResult.setText("");
            imageView.setImageURI(null);
        }

        @Override
        public void onPostExecute(byte[] result) {
            if (result == null) {
                Log.w(TAG, "Result is null, something went wrong with downloading");
                return;
            }

            imgResult = result;
            // is it an image?
            Bitmap bmp = BitmapFactory.decodeByteArray(result, 0, result.length);

//            MediaStore.Images.Media.insertImage(getContentResolver(), bmp, "title", "desc");
//            Toast.makeText(URLActivity.this, "Image saved", Toast.LENGTH_LONG).show();

            imageView.setImageBitmap(bmp);

            // change button back to "start download"
            btnStartCancel.setText("Start Download");
            // set progress bar to 0
            pbDownloadProgress.setProgress(0);
            // set downloaderAsyncTask back to null
            downloaderAsyncTask = null;
        }
    }


}
