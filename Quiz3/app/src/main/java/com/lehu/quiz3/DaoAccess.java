package com.lehu.quiz3;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface DaoAccess {

    @Insert
    void insertTrip(Trip trip);

    @Query("SELECT * FROM Trips")
    List<Trip> fetchAll();

    @Query("SELECT * FROM trips ORDER BY destination ASC")
    List<Trip> sortFriendsByNameAsc();

    @Query("SELECT * FROM trips ORDER BY dueDate DESC")
    List<Trip> sortFriendsByNameDesc();


}
