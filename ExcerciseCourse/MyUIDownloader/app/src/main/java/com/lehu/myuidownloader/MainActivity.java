package com.lehu.myuidownloader;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.toString();
    private static final String File_Name = "download.txt";

    DownloadAsyncTask downloadAsyncTask = null;
    EditText editTextURL = null;
    Button btnStartCancel;
    EditText etResult;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        downloadAsyncTask = new DownloadAsyncTask();
        editTextURL = (EditText) findViewById(R.id.etURL);
        btnStartCancel = (Button) findViewById(R.id.btStartCancel);
        etResult = (EditText) findViewById(R.id.etResult);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //save url form edit tx to preferences;
    }

    @Override
    protected void onStart() {
        super.onStart();
        //load url from preference int edit text
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            Log.d(TAG, "onStart: ");
        }
    }

    public void onClickStartCancel(View view) {
        Log.d(TAG, "onClickStartCancel: download content from url ");
        String url = editTextURL.getText().toString();
        try {
            URL urlObj = new URL(url);
            downloadAsyncTask = new DownloadAsyncTask();
            downloadAsyncTask.execute(urlObj);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    class DownloadAsyncTask extends AsyncTask<URL, Integer, Void> {
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected Void doInBackground(URL... urls) {
            Log.d(TAG, "doInBackground: download file>>>>>>>");
            URL url = urls[0];
            URLConnection connection;
//            StringBuilder result = new StringBuilder();
//            FileWriter fileWriter = null;
            FileOutputStream fileOutputStream = null;
            try {
                connection = url.openConnection();
                connection.connect();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));
                //write file;
//                fileWriter = new FileWriter(new File(File_Name));
                fileOutputStream = openFileOutput(File_Name, Context.MODE_PRIVATE);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);

                String s;
                int i = 0;
                while ((s = bufferedReader.readLine()) != null) {
//                    result.append(s);
                    bufferedWriter.write(s);
                    Thread.sleep(10);
                    publishProgress(i = i + 50);
                    if (isCancelled()) break;
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (fileOutputStream != null)
                        fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
//            return result.toString();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            Log.d(TAG, "onProgressUpdate: set progressbar value -----------------" + values[0]);
            progressBar.setProgress(values[0]);
        }

        @Override
        protected void onPreExecute() {
            Log.d(TAG, "onPreExecute: ");
            super.onPreExecute();

        }

        @Override
        protected void onCancelled() {
            Log.d(TAG, "onCancelled: ");
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(Void avoid) {
            Log.d(TAG, "onPostExecute: get result from URL");
            etResult.setText("succussfully");
        }

    }
}

