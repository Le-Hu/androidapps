package com.lehu.friendswithphotos;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.toString();
    public static final String FriendID = "id";
    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
    ListView lvFriendList;
    List<Friend> friendsList;
    ArrayAdapter<Friend> friendsAdapter;
    private AppDatabase appDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appDatabase = AppDatabase.getInstance(getApplicationContext());
        // init listview and init friendlist to show it at UI.
        lvFriendList = (ListView) findViewById(R.id.lvFriendList);
        friendsList = new ArrayList<>();
        //init adapter with a layout I created and a friendlist;
        friendsAdapter = new FriendArrayAdapter(this, friendsList);
        lvFriendList.setAdapter(friendsAdapter);


        // Long click event
        lvFriendList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                //show dialog  -> yes or no ? delete item what you deal with.;
                Log.d(TAG, "onItemLongClick: what's the long id: --> " + id);
                Friend selectedFriend = (Friend) parent.getItemAtPosition(position);
                showDeleteDia(selectedFriend);
                return true;
            }
        });

        // edit an item you selected , by short click;
        lvFriendList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Friend selectedFriend = (Friend) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, AddEditActivity.class);
                intent.putExtra(FriendID, selectedFriend.getId());
                startActivity(intent);
            }
        });
    }

    /**
     * delete item you selected;
     *
     * @param selectedFriend
     */
    private void showDeleteDia(final Friend selectedFriend) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("remove friend");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                friendList.remove(nameClicked);
//                friendsAdapter.notifyDataSetChanged();
                // delete async task;
                Log.d(TAG, "onClick: delete friend is " + selectedFriend);
                friendsList.remove(selectedFriend);
                new DeleteFriendAsyncTask().execute(selectedFriend);
                friendsAdapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add:
                Log.d(TAG, "onOptionsItemSelected: item of add is click");
                //todo: create an new acitivy to add a new friend;
                Intent intent = new Intent(this, AddEditActivity.class);
                startActivity(intent);
                break;
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        new SortFriendsByNameDescDbAsyncTask().execute();
    }

    class FriendArrayAdapter extends ArrayAdapter<Friend> {

        private Context context;
        private List<Friend> list;

        public FriendArrayAdapter(Context context, List<Friend> arraylist) {
            super(context, R.layout.friend_item, arraylist);
            this.context = context;
            this.list = arraylist;
        }


        @SuppressLint("SetTextI18n")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View viewTask = convertView;
            if (viewTask == null) {
                viewTask = inflater.inflate(R.layout.friend_item, parent, false);
            }
            //View viewTask = inflater.inflate(R.layout.todolist_item, parent, false);
            ImageView ivIcon = (ImageView) viewTask.findViewById(R.id.ivPhoto);
            TextView tvFirstLine = (TextView) viewTask.findViewById(R.id.tvFirstLastName);
            TextView tvSecondLine = (TextView) viewTask.findViewById(R.id.tvDateOfBirth);
            //
            Friend friend = list.get(position);
            tvFirstLine.setText(friend.getFirstName() + friend.getLastName());
            String dueDate = "Due: " + simpleDateFormat.format(new Date(friend.getDateOfBirth()));
            tvSecondLine.setText(dueDate);
            if (friend.getImageFilePath() != null) {
                Uri bitmap = Uri.fromFile(new File(friend.getImageFilePath()));
                ivIcon.setImageURI(bitmap);
            }
            return viewTask;
        }
    }

    class SortFriendsByNameDescDbAsyncTask extends AsyncTask<Void, Void, List<Friend>> {
        @Override
        protected List<Friend> doInBackground(Void... params) {
            try {
                return appDatabase.daoAccess().sortFriendsByNameDesc();
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Friend> result) {
//            friendsList = result;
            friendsList.clear();
            friendsList.addAll(result);
            friendsAdapter.notifyDataSetChanged();
        }
    }

    class DeleteFriendAsyncTask extends AsyncTask<Friend, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Friend... integers) {
            // delete a friend from db;
            try {
                appDatabase.daoAccess().deleteFriend(integers[0]);
                return true;
            } catch (Exception ex) {
                return false;
            }
        }
    }
}
