package com.lehu.friendswithphotos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface DaoAccess {

    @Insert
    void insertFriend(Friend friend);

    @Insert
    void insertMultipleFriends(List<Friend> friendList);

    @Query("SELECT * FROM friends WHERE id = :friendId")
    Friend fetchFriendById(int friendId);

    @Query("SELECT * FROM friends")
    List<Friend> fetchAllFriends();

    @Query("SELECT * FROM friends ORDER BY firstName ASC")
    List<Friend> sortFriendsByNameAsc();

    @Query("SELECT * FROM friends ORDER BY firstName DESC")
    List<Friend> sortFriendsByNameDesc();

    @Update
    void updateFriend(Friend friend);

    @Delete
    void deleteFriend(Friend friend);

    @Query("DELETE FROM friends")
    void deleteAllFriends();
}
