package com.lehu.friendsdbroom;

import android.arch.persistence.room.Room;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;


import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String DATABASE_NAME = "friends_db";
    private AppDatabase appDatabase;

    ListView lvFriends;
    List<String> friendList = new ArrayList<>();
    ArrayAdapter<String> friendsAdapter;
    public static final String TAG = "MainActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DATABASE_NAME).fallbackToDestructiveMigration().build();


        friendsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1, friendList);
        // this.loadFriendsFromFile();
        if (friendList == null) {
            friendList = new ArrayList<>();
        }
        lvFriends.setAdapter(friendsAdapter);


    }


    private void modify(final long index) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Modify name");
        final EditText editText = new EditText(this);
        builder.setView(editText);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String itemSelected = editText.getText().toString();
                friendList.set((int) index, itemSelected);
                friendsAdapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void deleteFriend(final String nameClicked) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("remove friend");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                friendList.remove(nameClicked);
                friendsAdapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.mi_add: {
                this.showAddFriendDialog(null);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void showAddFriendDialog(final String editedName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add friend");

        final EditText editText = new EditText(this);
        editText.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(editText);

        builder.setPositiveButton(editedName == null ? "Add" : "Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = editText.getText().toString();
                if (editedName == null) { // add
                    appDatabase.daoAccess().insertFriend(new Friend(0, name));
                } else { // save
                    int index = friendList.indexOf(editedName);
                    // FIXME
                }
                //appDatabase.daoAccess().insertFriend(new Friend(0, name));
                //friendList.add(name);
                friendsAdapter.notifyDataSetChanged();
                Toast.makeText(MainActivity.this, "frend added", Toast.LENGTH_LONG).show();
            }
        });
        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

//    private void saveFriendsToFile() {
//        FileOutputStream fileOutputStream = null;
//        try {
//            fileOutputStream = openFileOutput(FileName, Context.MODE_PRIVATE);
//            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
//            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
//            for (String string : friendList) {
//                bufferedWriter.write(string);
//                bufferedWriter.write("\n");
//            }
//
//            bufferedWriter.close();
//            outputStreamWriter.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (fileOutputStream != null) {
//                try {
//                    fileOutputStream.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//    }
//
//    private void loadFriendsFromFile() {
//        FileInputStream fileInputStream = null;
//        try {
//            fileInputStream = openFileInput(FileName);
//            InputStreamReader bufferedInputStream = new InputStreamReader(fileInputStream);
//            BufferedReader bf = new BufferedReader(bufferedInputStream);
//            String line;
////            friendList = new ArrayList<>();
//            friendList.clear();
//            while ((line = bf.readLine()) != null) {
//                friendList.add(line);
//            }
//            bf.close();
//            bufferedInputStream.close();
//        } catch (FileNotFoundException e) {
//            Log.d(TAG, "loadFriendsFromFile: there is no file!");
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (fileInputStream != null) {
//                try {
//                    fileInputStream.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart: load file");
        super.onStart();
//        this.loadFriendsFromFile();
//        friendsAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop: write file! ");
        super.onStop();
//        this.saveFriendsToFile();
    }

    class LoadFriendFromDbAsyncTask extends AsyncTask<Void, Void, List<Friend>> {

        @Override
        protected List<Friend> doInBackground(Void... voids) {
            return null;
        }
    }

}
