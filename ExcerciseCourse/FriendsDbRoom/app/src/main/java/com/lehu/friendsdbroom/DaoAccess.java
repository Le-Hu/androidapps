package com.lehu.friendsdbroom;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.widget.ListView;

import java.util.List;

@Dao
public interface DaoAccess {
    @Insert
    void insertFriend(Friend friend);

    @Insert
    void insertMultipleFriends(List<Friend> friendsList);

    @Query("SELECT*FROM Friends WHERE id =:friendId")
    Friend fetchOneFriendId(int friendId);

    @Query("SELECT * FROM Friends")
    List<Friend> fetchAllFriens();

    @Update
    void updateMovie(Friend friend);

    @Delete
    void deleteMovie(Friend friend);
}
