package com.lehu.friendsdbroom;

import android.annotation.SuppressLint;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.view.OrientationEventListener;

@Entity(tableName = "Friends")
public class Friend {

    @NonNull
    @PrimaryKey
    private int id;

    private String name;

    public Friend(int i, String name) {
        this.setId(i);
        this.name = name;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%d: %s", id, name);
    }
}
