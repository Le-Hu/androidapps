package com.example.mobileapps.notepad;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Get the shared preference  
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        etPassword = (EditText) findViewById(R.id.etPassword);
    }

    public void onClickLogin(View view) {
        String inputPassword = etPassword.getText().toString();
        String storedPassword = sharedPreferences.getString("prefUserPassword", "");

        if (inputPassword.equals(storedPassword)) {
            Intent intent = new Intent(this, NotepadActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Invalid password", Toast.LENGTH_SHORT).show();
        }
    }
}
