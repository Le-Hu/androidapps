package com.example.mobileapps.notepad;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class NotepadActivity extends AppCompatActivity {

    public static final String FILE_NAME = "notepad_data.txt";
    public static final String TAG = "NotepadActivity";
    EditText etNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notepad);

        etNote = (EditText) findViewById(R.id.etNote);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.notepad_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.miSettings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        readFile();
    }

    @Override
    public void onResume() {
        super.onResume();
        readFile();
    }

    public void readFile() {
        FileInputStream fileInputStream;
        Scanner fileInput = null;

        try {
            fileInputStream = openFileInput(FILE_NAME);
            fileInput = new Scanner(fileInputStream);
            fileInput.useDelimiter("\\z");
            String note;
            if (fileInput.hasNext()) {
                note = fileInput.next();
            } else {
                return;
            }

            fileInputStream.close();
            etNote.setText(note);
        } catch (FileNotFoundException ex) {
            //
        } catch (IOException ex) {
            Toast.makeText(this, "Error reading text from file", Toast.LENGTH_LONG).show();
        } finally {
            if (fileInput != null) {
                fileInput.close();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        saveFile();
    }

    @Override
    public void onPause() {
        super.onPause();

        saveFile();
    }

    public void saveFile() {
        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            String note = etNote.getText().toString();
            fileOutputStream.write(note.getBytes());
            Log.d(TAG, "File contents written");
        } catch (IOException ex) {
            Toast.makeText(this, "Error saving text to file", Toast.LENGTH_SHORT).show();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException ex) {
                    //
                }
            }
        }
    }
}
