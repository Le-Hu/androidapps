package com.lehu.friendrestclient;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String BASE_API_URL = "http://10.0.2.2:8801/api.php";


    List<Friend> friendsList = new ArrayList<>();
    ArrayAdapter<Friend> friendArrayAdapter;
    ListView lvFriends;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvFriends = findViewById(R.id.lvFriens);
        friendArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, friendsList);
        lvFriends.setAdapter(friendArrayAdapter);

    }

    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        // return super.onCreatePanelMenu(featureId, menu);

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add:
                //todo: add friend by restapi;
                break;
        }
        return true;
    }


    class LoadFriendsListAsyncTask extends AsyncTask<Void, Void, List<Friend>> {
        public String TAG = LoadFriendsListAsyncTask.class.toString();

        @Override
        protected List<Friend> doInBackground(Void... voids) {
            InputStreamReader input = null;
            JsonReader reader = null;
            try {
                Log.d(TAG, "doInBackground: Get /friends");
                List<Friend> result = new ArrayList<>();
                URL url = new URL(MainActivity.BASE_API_URL + "/friends");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                input = new InputStreamReader(connection.getInputStream());
                int httpCode = connection.getResponseCode();

                if (httpCode / 100 != 2) {
                    throw new IOException("invalid http code response: " + httpCode);
                }
                reader = new JsonReader(input);

                return result;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        //Todo: Issue why result can't assign to friends list directly;
        protected void onPostExecute(List<Friend> result) {
            friendsList.clear();
            if (result == null) {
                Toast.makeText(MainActivity.this, "API error fectching data", Toast.LENGTH_LONG).show();
                return;
            }
            friendsList.addAll(result);
            friendArrayAdapter.notifyDataSetChanged();
        }
    }

}
