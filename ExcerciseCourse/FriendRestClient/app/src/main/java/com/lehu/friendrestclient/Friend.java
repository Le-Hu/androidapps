package com.lehu.friendrestclient;

public class Friend {

    private int id;
    private String name;
    private int age;


    public Friend(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }


    @Override
    public String toString() {
        return String.format("%d: %s is %d y/o", id, name, age);
    }
}
