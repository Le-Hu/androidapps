package com.example.mobileapps.friendslist;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView lvFriends;
    ArrayList<String> friendList = new ArrayList<>();
    ArrayAdapter<String> friendsAdapter;
    public static final String TAG = "MainActivity";
    //    String[] friendsInitList = {"Lujia", "AnLin", "LeHu", "Taka", "AdNess", "Corolin"};
    public static final String FileName = "namelist.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //
        lvFriends = (ListView) findViewById(R.id.lvFriends);

        //friendList = new ArrayList<>();
//        for (String name : friendsInitList) {
//            friendList.add(name);
//        }

        friendsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1, friendList);
        this.loadFriendsFromFile();
        if (friendList == null) {
            friendList = new ArrayList<>();
        }
        lvFriends.setAdapter(friendsAdapter);


//        lvFriends.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                String nameClicked = (String) parent.getItemAtPosition(position);
//                friendList.remove(nameClicked);
//                friendsAdapter.notifyDataSetChanged();
//
//                Toast.makeText(MainActivity.this, "zzz", Toast.LENGTH_LONG).show();
//            }
//        });

        lvFriends.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                String nameClicked = (String) parent.getItemAtPosition(position);
                deleteFriend(nameClicked);
                return true;
            }
        });

        lvFriends.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long index = parent.getItemIdAtPosition(position);
//                Log.d(TAG, "onItemClick: itemId = " + parent.getItemIdAtPosition(position));
                modify(index);
            }
        });
    }

    private void modify(final long index) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Modify name");
        final EditText editText = new EditText(this);
        builder.setView(editText);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String itemSelected = editText.getText().toString();
                friendList.set((int) index, itemSelected);
                friendsAdapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void deleteFriend(final String nameClicked) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("remove friend");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                friendList.remove(nameClicked);
                friendsAdapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.mi_add: {
                this.showAddFriendDialog();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void showAddFriendDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add friend");

        final EditText editText = new EditText(this);
        editText.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(editText);

        builder.setPositiveButton("add friend", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = editText.getText().toString();
                friendList.add(name);
                friendsAdapter.notifyDataSetChanged();
                Toast.makeText(MainActivity.this, "frend added", Toast.LENGTH_LONG).show();
                Log.d(TAG, "onClick: Friend added" + name);
            }
        });
        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void saveFriendsToFile() {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = openFileOutput(FileName, Context.MODE_PRIVATE);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
            for (String string : friendList) {
                bufferedWriter.write(string);
                bufferedWriter.write("\n");
            }

            bufferedWriter.close();
            outputStreamWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void loadFriendsFromFile() {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = openFileInput(FileName);
            InputStreamReader bufferedInputStream = new InputStreamReader(fileInputStream);
            BufferedReader bf = new BufferedReader(bufferedInputStream);
            String line;
//            friendList = new ArrayList<>();
            friendList.clear();
            while ((line = bf.readLine()) != null) {
                friendList.add(line);
            }
            bf.close();
            bufferedInputStream.close();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "loadFriendsFromFile: there is no file!");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart: load file");
        super.onStart();
//        this.loadFriendsFromFile();
//        friendsAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop: write file! ");
        super.onStop();
        this.saveFriendsToFile();
    }
}
