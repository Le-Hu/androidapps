package com.example.mobileapps.mytodo;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Todo implements Serializable {
    String task;
    Date duDate;
    boolean isDone;

    public Todo(String task, Date duDate, boolean isDone) {
        this.task = task;
        this.duDate = duDate;
        this.isDone = isDone;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public Date getDuDate() {
        return duDate;
    }

    public void setDuDate(Date duDate) {
        this.duDate = duDate;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    @Override
    public String toString() {
        return "Todo{" +
                "task='" + task + '\'' +
                ", duDate=" + duDate +
                ", isDone=" + isDone +
                '}';
    }

    public String printLine() {
        return this.getTask() + "," + this.getDuDate().getTime() + "," + isDone + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Todo todo = (Todo) o;
        return isDone == todo.isDone &&
                Objects.equals(task, todo.task) &&
                Objects.equals(duDate, todo.duDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(task, duDate, isDone);
    }
}

