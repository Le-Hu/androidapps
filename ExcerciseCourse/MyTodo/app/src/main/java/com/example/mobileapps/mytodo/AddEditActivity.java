package com.example.mobileapps.mytodo;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CalendarView;
import android.widget.Switch;
import android.widget.TextView;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class AddEditActivity extends AppCompatActivity {
    private static final String TAG = AddEditActivity.class.toString();
    TextView tvTask;
    CalendarView calendar;
    Switch isDone;
    Date dateSelected;
    Todo currentEditingTodo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: " + AddEditActivity.class);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit);

        tvTask = (TextView) findViewById(R.id.etTask);
        isDone = (Switch) findViewById(R.id.btSwitch);
        //calendar add dataChangeSelected lisener;
        calendar = (CalendarView) findViewById(R.id.calendarView);
        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int day) {
                dateSelected = new Date(year, month, day);
                Log.d(TAG, "onSelectedDayChange: date ===" + dateSelected);
            }
        });

        getEditTode();
        //init add or edit;
        dateSelected = new Date(calendar.getDate());
    }

    private void getEditTode() {
        Intent intent = getIntent();
        if (intent != null) {
            currentEditingTodo = (Todo) intent.getSerializableExtra(MainActivity.CurrentTodo);
            if (currentEditingTodo != null) {
                tvTask.setText(currentEditingTodo.getTask());
                isDone.setChecked(currentEditingTodo.isDone);
                calendar.setDate(currentEditingTodo.getDuDate().getTime());
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        if (currentEditingTodo == null) {
            menuInflater.inflate(R.menu.add_menu, menu);
        } else {
            menuInflater.inflate(R.menu.edit_menu, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add: {
                Todo todo = new Todo(tvTask.getText().toString(), dateSelected, isDone.isChecked());
                MainActivity.todoList.add(todo);
                saveTasktoFile();
                finish();
                return true;
            }
            case R.id.miSave: {
                Log.d(TAG, "onOptionsItemSelected: save");
                int index = MainActivity.todoList.indexOf(currentEditingTodo);
                currentEditingTodo.setTask(tvTask.getText().toString());
                currentEditingTodo.setDuDate(dateSelected);
                currentEditingTodo.setDone(isDone.isChecked());
                MainActivity.todoList.set(index, currentEditingTodo);
                saveTasktoFile();
                finish();
                return true;
            }
            case R.id.mtDelete: {
                Log.d(TAG, "onOptionsItemSelected: delete ");
                int index = MainActivity.todoList.indexOf(currentEditingTodo);
                MainActivity.todoList.remove(index);
                saveTasktoFile();
                finish();
                return true;
            }
            default:
                //fixme:
        }
        return super.onOptionsItemSelected(item);
    }


    private void saveTasktoFile() {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = openFileOutput(MainActivity.FileName, Context.MODE_PRIVATE);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);

            for (Todo todo : MainActivity.todoList) {
                String todoStr = todo.printLine();
                bufferedWriter.write(todoStr);
//                bufferedWriter.write("\n");
            }
            bufferedWriter.close();
            outputStreamWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
