package com.example.mobileapps.mytodo;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.toString();
    public static final String CurrentTodo = "CurrentEditingTodo";
    public static final String FileName = "todo.txt";

    public static ArrayList<Todo> todoList = new ArrayList<>();

//    static {
//        todoList.add(new Todo("buy milk", new Date(), false));
//        todoList.add(new Todo("buy water", new Date(), true));
//    }

    ListView lvTodos;
    ArrayAdapter<Todo> todoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: " + MainActivity.class);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        lvTodos = (ListView) findViewById(R.id.lvTodos);
        todoAdapter = new TodoArrayAdapter(this, todoList);
        //add ItemClick listener for editing a todo obj
        lvTodos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //get current item clicked;
                Todo todo = (Todo) adapterView.getItemAtPosition(i);
                Intent intent = new Intent(MainActivity.this, AddEditActivity.class);
                Log.d(TAG, "onItemClick: >>>> todo : " + todo);
                intent.putExtra(CurrentTodo, todo);
                startActivity(intent);

            }
        });
        lvTodos.setAdapter(todoAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.miAdd: {
                Log.d(TAG, "onOptionsItemSelected: Add Task ");
                Intent intent = new Intent(this, AddEditActivity.class);
                startActivity(intent);
                return true;
            }
        }
        return true;
    }

    public class TodoArrayAdapter extends ArrayAdapter<Todo> {
        Context context;
        ArrayList<Todo> list;

        public TodoArrayAdapter(Context context, ArrayList<Todo> list) {
            super(context, R.layout.todo_item, list);
            this.context = context;
            this.list = list;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.todo_item, parent, false);
            TextView tvFirstLine = (TextView) rowView.findViewById(R.id.tvFirstLine);
            TextView tvSecondLine = (TextView) rowView.findViewById(R.id.tvSecondLine);
            ImageView ivIcon = (ImageView) rowView.findViewById(R.id.ivIcon);

            Todo todo = list.get(position);
            tvFirstLine.setText(todo.task);
            tvSecondLine.setText(todo.duDate.toString());//todo: format date;
            Log.d(TAG, "getView: ivIcon " + todo.isDone);
            ivIcon.setImageResource(todo.isDone ? android.R.drawable.star_on : android.R.drawable.star_off);

            return rowView;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadTasksFromFile();
        todoAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveTasktoFile();
    }

    private void saveTasktoFile() {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = openFileOutput(FileName, Context.MODE_PRIVATE);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
            for (Todo todo : todoList) {
                String todoStr = todo.printLine();
                bufferedWriter.write(todoStr);
//                bufferedWriter.write("\n");
            }
            bufferedWriter.close();
            outputStreamWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void loadTasksFromFile() {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = openFileInput(FileName);
            InputStreamReader bufferedInputStream = new InputStreamReader(fileInputStream);
            BufferedReader bf = new BufferedReader(bufferedInputStream);
            String line;
            todoList.clear();
            while ((line = bf.readLine()) != null) {
                String[] splist = line.split(",");
                boolean isDone = true;
                if (splist[2].equalsIgnoreCase("false")) {
                    isDone = false;
                }
                Todo todo = new Todo(splist[0], new Date(Long.parseLong(splist[1])), isDone);
                todoList.add(todo);
            }
            bf.close();
            bufferedInputStream.close();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "loadTasksFromFile: there is no file!");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
