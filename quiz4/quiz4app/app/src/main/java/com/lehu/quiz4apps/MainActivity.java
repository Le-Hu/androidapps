package com.lehu.quiz4apps;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String BASE_API_URL = "http://10.0.2.2:8888/api.php";

    private static final String TAG = MainActivity.class.toString();
    public static final String EXTRA_ItemId = "ITEMID";
    ListView lvProduct;
    List<Item> itemsList;
    ArrayAdapter<Item> itemAdapter;
//    int itemId;

    public static int currentUserId = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        //
    }

    /**
     * init UI; and add listener
     */
    private void init() {
        lvProduct = findViewById(R.id.lvItems);
        itemsList = new ArrayList<>();
        itemAdapter = new ProductArrayAdapter(this, itemsList);
        lvProduct.setAdapter(itemAdapter);
        editToyClickListener();
    }


    @Override
    protected void onStart() {
        super.onStart();
        new LoadToyListAsyncTask().execute();
    }

    /**
     * Inner class for fetching data from remote MySQL
     */
    class LoadToyListAsyncTask extends AsyncTask<Void, Void, ArrayList<Item>> {

        @Override
        protected ArrayList<Item> doInBackground(Void... voids) {
            return getDataFromHttpRequest("GET", "/items");
        }

        @Override
        protected void onPostExecute(ArrayList<Item> result) {
            refreshToysList(result);
        }
    }

    /**
     * Refresh the results of toy list from database
     *
     * @param list
     */
    public void refreshToysList(List<Item> list) {
        itemsList.clear();

        if (list == null) {
            Toast.makeText(getApplicationContext(), "API error fetching data", Toast.LENGTH_LONG).show();
            return;
        }

        itemsList.addAll(list);
        itemAdapter.notifyDataSetChanged();
    }

    /**
     * fetch data of item ...list
     *
     * @param httpRequest
     * @param restfulUrl
     * @return
     */
    public ArrayList<Item> getDataFromHttpRequest(String httpRequest, String restfulUrl) {

        String TAG = "CRUDToyListAsyncTask";

        InputStreamReader inputStreamReader;
        JsonReader jsonReader;

        try {
            Log.v(TAG, httpRequest + " / items");

            ArrayList<Item> result = new ArrayList<>();
            URL url = new URL(MainActivity.BASE_API_URL + restfulUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(httpRequest);
            inputStreamReader = new InputStreamReader(conn.getInputStream());

            int httpCode = conn.getResponseCode();
            if (httpCode / 100 != 2) { // Not 2xx means issues
                throw new IOException("Invalid HTTP Code response: " + httpCode);
            }

            jsonReader = new JsonReader(inputStreamReader);
            jsonReader.beginArray();

            while (jsonReader.hasNext()) {
                String key;
                jsonReader.beginObject();

                // id from friendsdb.friends in MySQL
                key = jsonReader.nextName();
                if (!key.equals("id")) throw new ParseException("id expected when parsing", 0);
                int id = jsonReader.nextInt();

                // name from friendsdb.friends in MySQL
                key = jsonReader.nextName();
                if (!key.equals("description"))
                    throw new ParseException("description expected when parsing", 0);
                String desc = jsonReader.nextString();

                // age from friendsdb.friends in MySQL
                key = jsonReader.nextName();
                if (!key.equals("highestBid"))
                    throw new ParseException("highestBid expected when parsing", 0);
                //Toy.ToyType type = Toy.ToyType.valueOf(jsonReader.nextString());
                String highestBid = jsonReader.nextString();
                BigDecimal highBid = new BigDecimal(highestBid);

                jsonReader.endObject();
                //Toy toy = new Toy(id, name, type);
                Item item = new Item();
                item.setId(id);
                item.setDescription(desc);
                item.setHighestBid(highBid);

                result.add(item);

                Log.v(TAG, "Toy parsed and added to list: " + lvProduct.toString());
            }

            jsonReader.endArray();

            return result;
        } catch (IOException | ParseException ex) {
            Log.e(TAG, "Exception reading from URL", ex);
            return null;
        }
    }


    public void editToyClickListener() {
        // Anonymous inner class with event handler
        lvProduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // ItemView was clicked, find out which String (name) was displaying
                Item selected = (Item) parent.getItemAtPosition(position);
                int itemId = selected.getId();
                editActivity(itemId);
            }
        });
    }


    private void editActivity(int itemId) {
        Log.d(TAG, "onItemClick start edit activity: item id = " + itemId);
        Intent intent = new Intent(this, EditActivity.class);
        intent.putExtra(MainActivity.EXTRA_ItemId, itemId);
        startActivity(intent);

//        String toy = selected.getDescription() + "\n" + selected.getHighestBid();
//        Toast.makeText(getApplicationContext(), toy, Toast.LENGTH_LONG).show();
//        Log.d(TAG, "Selected Toy \n========================================================\n"
//                + toy + "\n========================================================");
    }


    //inner class;
    class ProductArrayAdapter extends ArrayAdapter<Item> {

        private Context context;
        private List<Item> list;

        public ProductArrayAdapter(Context context, List<Item> list) {
            super(context, R.layout.product_item, list);
            this.context = context;
            this.list = list;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View viewToy = convertView;
            if (viewToy == null) {
                viewToy = inflater.inflate(R.layout.product_item, parent, false);
            }

            ImageView ivIcon = (ImageView) viewToy.findViewById(R.id.ivIcon);
            TextView tvFirstLine = (TextView) viewToy.findViewById(R.id.tvFirstLine);
            TextView tvSecondLine = (TextView) viewToy.findViewById(R.id.tvSecondLine);

            Item item = list.get(position);

            //copy man.jpg file from
            ivIcon.setImageResource(R.drawable.man);

            String name = item.getDescription();
            tvFirstLine.setText(name);

            String type = String.valueOf(item.getHighestBid());
            tvSecondLine.setText(type);

            return viewToy;
        }
    }


    //-------------for register

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add:

                if (currentUserId == -1) {
                    Intent intent = new Intent(this, AccountActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(this, AddActivity.class);
                    startActivity(intent);
                }

                return true;
            case R.id.mi_account:

                Intent intent = new Intent(this, AccountActivity.class);
                startActivity(intent);
//                toggleSortToys();
//                toggleSortToys();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
