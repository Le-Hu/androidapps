package com.lehu.quiz4apps;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class EditActivity extends AppCompatActivity {

    private static final String TAG = EditActivity.class.toString();
    EditText eTprice;
    EditText eTemail;
    Button btnBid;
    int itemId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        //init UI
        eTprice = findViewById(R.id.etBidPrice);
        eTemail = findViewById(R.id.etEmail);
        btnBid = findViewById(R.id.btnBid);

        Intent intent = getIntent();
        itemId = intent.getIntExtra(MainActivity.EXTRA_ItemId, -1);
//        Log.d(TAG, " edit itemId: " + itemId);
//        Log.d(TAG, "onCreate: item id = " + intent.getStringExtra(MainActivity.EXTRA_ItemId));
//        itemId = Integer.valueOf(intent.getStringExtra(MainActivity.EXTRA_ItemId));
    }

    // update price ; for item id
    public void onClickBid(View view) {
        Item item = new Item();

        item.setId(itemId);
        item.setHighestBid(new BigDecimal(eTprice.getText().toString()));
        item.setHighestBidderEmail(eTemail.getText().toString());

        new UpdateAsyncTask().execute(item);
        finish();

    }


    class UpdateAsyncTask extends AsyncTask<Item, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Item... items) {
            Item item = items[0];

            HttpURLConnection conn = null;
            PrintWriter printWriter = null;

            try {
//                Log.d(TAG, "doInBackground: ");
                URL url = new URL(MainActivity.BASE_API_URL + "/item/" + item.getId());
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("PUT");
                conn.setDoInput(false); // Not receiving data
                conn.setDoOutput(true); // But sending data
                //
                printWriter = new PrintWriter(conn.getOutputStream());

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("highestBid", item.getHighestBid().toString());
                jsonObject.put("highestBidderEmail", item.getHighestBidderEmail());

                String stringJson = jsonObject.toString();

                printWriter.print(stringJson);
                printWriter.flush();
                printWriter.close();

                int httpCode = conn.getResponseCode();
                if (httpCode / 100 != 2)
                    throw new IOException("Invalid HTTP Code response: " + httpCode);
                return true;
            } catch (IOException | JSONException ex) {
//                Log.e(TAG, "Exception writing to URL", ex);
                return false;
            } finally { // FIXME: finally missing closing print writer. connection
                if (printWriter != null) {
                    printWriter.close();
                    conn.disconnect();
                }
            }
        }
    }
}
