package com.lehu.quiz4apps;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class AccountActivity extends AppCompatActivity {


    EditText etEmail, etPwd;
    Button btnRegister,btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        etEmail = findViewById(R.id.etEmail);
        etPwd = findViewById(R.id.etPwd);
        btnLogin = findViewById(R.id.btnLogin);
        btnRegister = findViewById(R.id.btnRegister);



    }


    public void onClickLogin(View view) {

        User user = new User();
        user.setEmail(etEmail.getText().toString());
        user.setPassword(etPwd.getText().toString());

        new InsertUserAsyncTask().execute(user);



    }


    public void onClickRegister(View view){

    }



    class InsertUserAsyncTask extends AsyncTask<User, Void, Integer> {

        public static final String TAG = "InsertToyAsyncTask";

        @Override
        protected Integer doInBackground(User... params) {

            User user = params[0];
            HttpURLConnection conn = null;
            PrintWriter printWriter = null;

            try {
                Log.v(TAG, "POST / user");

                URL url = new URL(MainActivity.BASE_API_URL + "/user");
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setDoInput(false); // Not receiving data
                conn.setDoOutput(true); // But sending data

                //write
                printWriter = new PrintWriter(conn.getOutputStream());

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("email",user.getEmail());
                jsonObject.put("password", user.getPassword());

                String stringJson = jsonObject.toString();

                printWriter.print(stringJson);
                printWriter.flush();
                printWriter.close();

                int httpCode = conn.getResponseCode();
                if (httpCode / 100 != 2) throw new IOException("Invalid HTTP Code response: " + httpCode);

                String msg = conn.getResponseMessage();

                return Integer.valueOf(msg);

            } catch (IOException | JSONException ex) {
                Log.e(TAG, "Exception writing to URL", ex);
                return -1;
            } finally { // FIXME: finally missing closing print writer. connection
                if (printWriter != null) {
                    printWriter.close();
                    conn.disconnect();
                }
            }

        }

        @Override
        protected void onPostExecute(Integer result) {

            Toast.makeText(getApplicationContext(), "result = "+result, Toast.LENGTH_LONG).show();

//            if (result) {
//                Toast.makeText(getApplicationContext(), "Toy added successful", Toast.LENGTH_LONG).show();
//                finish();
//            } else {
//                Toast.makeText(getApplicationContext(), "API error adding friend", Toast.LENGTH_LONG).show();
//            }

        }
    }

}
