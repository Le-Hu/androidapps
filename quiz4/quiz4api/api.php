<?php
/**
 * Created by PhpStorm.
 * User: mobileapps
 * Date: 2018-12-05
 * Time: 15:20
 */


require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));


DB::$dbName = 'quiz4db';
DB::$user = 'quiz4db';
DB::$password = 'quiz4db';
DB::$host = 'localhost'; // localhost'; // try 127.0.0.1 if doesn't work on mac
DB::$port = 3333;

DB::$encoding = 'utf8';

DB::$error_handler = 'error_handler';
DB::$nonsql_error_handler = 'error_handler';





$app = new \Slim\Slim();
// this script will always return JSON to any client
$app->response()->header('content-type', 'application/json');

/**
 * show all items on mainactivity;
 */
$app->get('/items', function () {
    $item = DB::query("SELECT id,description,highestBid FROM items");
    echo json_encode($item, JSON_PRETTY_PRINT);
});

/**
 * put update price for a item , for items table;
 */
$app->put('/item/:itemId', function($itemId) use($app) {

    $json = $app->request()->getBody();
    $data = json_decode($json, true);

    DB::update('items', $data, 'id=%d', $itemId);
    echo json_encode(true);
});


$app->post('/user', function () use($app) {
    $json = $app->request()->getBody();
    $data = json_decode($json, true);

    DB::insert('users', $data);
    $app->response()->status(201);  // HTTP Status should be 201
    $id = DB::insertId();
    //$log->debug("todo created with id=" . $id);
    echo json_encode($id);
});


$app->run();
