package com.example.mobileapps.quiztwotrip;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Trip {
    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd");
    private String from;
    private String to;
    private Date date;
    private String transport;
    private int budge;

    public Trip() {

    }

    public Trip(String from, String to, Date date, String transport, int budge) {
        this.from = from;
        this.to = to;
        this.date = date;
        this.transport = transport;
        this.budge = budge;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTransport() {
        return transport;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    public int getBudge() {
        return budge;
    }

    public void setBudge(int budge) {
        this.budge = budge;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", date=" + date +
                ", transport='" + transport + '\'' +
                ", budge=" + budge +
                '}';
    }

    public String getTripLine() {
        return "From: " + this.getFrom() + "--" + "To: " + this.getTo();
    }

    public String printLine() {
        return this.from + "," + this.to + "," + simpleDateFormat.format(this.date) + "," + this.transport + "," + this.budge + "\n";
    }
}
