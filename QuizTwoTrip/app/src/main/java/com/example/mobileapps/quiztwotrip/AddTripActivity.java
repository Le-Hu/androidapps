package com.example.mobileapps.quiztwotrip;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;

public class AddTripActivity extends AppCompatActivity {

    private static final String TAG = AddTripActivity.class.toString();

    EditText etFrom, etTo, etDate, etBudget;
    RadioGroup rgTransport;
    SeekBar seekBarPrice;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_trip);
        //init;
        etFrom = (EditText) findViewById(R.id.etFrom);
        etTo = (EditText) findViewById(R.id.etTo);
        etDate = (EditText) findViewById(R.id.etDate);
        rgTransport = (RadioGroup) findViewById(R.id.rgTransport);
        seekBarPrice = (SeekBar) findViewById(R.id.seekBarPrice);

//        seekBarPrice.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//            @Override
//            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//
//            }
//
//            @Override
//            public void onStartTrackingTouch(SeekBar seekBar) {
//
//            }
//
//            @Override
//            public void onStopTrackingTouch(SeekBar seekBar) {
//
//            }
//        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.trip_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.mi_add_trip: {
                Log.d(TAG, "onOptionsItemSelected: Add Trip--");
                saveTrip();
                saveTasktoFile();
                finish();
                return true;
            }
        }
        return true;

    }

    private void saveTrip() {
        Trip trip = new Trip();
        trip.setFrom(etFrom.getText().toString());
        trip.setTo(etTo.getText().toString());
//        Log.d(TAG, "saveTrip: date format : yyyy-MM-dd" + etDate.getText().toString());
        String dateStr = etDate.getText().toString();
        String[] split = dateStr.split("-");
        Date tmp = new Date(Integer.parseInt(split[0]), Integer.parseInt(split[1]), Integer.parseInt(split[2]));
        trip.setDate(tmp);
        trip.setTransport(((RadioButton) findViewById(rgTransport.getCheckedRadioButtonId())).getText().toString());
        trip.setBudge(seekBarPrice.getProgress());

        MainActivity.tripList.add(trip);
    }


    private void saveTasktoFile() {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = openFileOutput(MainActivity.FileName, Context.MODE_PRIVATE);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
            for (Trip trip : MainActivity.tripList) {
                String todoStr = trip.printLine();
                bufferedWriter.write(todoStr);
//                bufferedWriter.write("\n");
            }
            bufferedWriter.close();
            outputStreamWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
