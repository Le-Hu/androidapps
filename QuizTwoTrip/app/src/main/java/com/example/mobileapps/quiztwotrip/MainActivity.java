package com.example.mobileapps.quiztwotrip;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.toString();
    public static final String FileName = "trip.txt";

    ListView lvTrips;
    ArrayAdapter<Trip> tripAdapter;

    public static ArrayList<Trip> tripList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvTrips = (ListView) findViewById(R.id.lvTrip);
        tripAdapter = new TodoArrayAdapter(this, tripList);


        lvTrips.setAdapter(tripAdapter);

    }

    //menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add: {
//                Log.d(TAG, "onOptionsItemSelected: Add Task ");
                Intent intent = new Intent(this, AddTripActivity.class);
                startActivity(intent);
                return true;
            }
        }
        return true;
    }

    public class TodoArrayAdapter extends ArrayAdapter<Trip> {
        Context context;
        ArrayList<Trip> list;

        public TodoArrayAdapter(Context context, ArrayList<Trip> list) {
            super(context, R.layout.trip_item, list);
            this.context = context;
            this.list = list;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.trip_item, parent, false);
            //get 3 view inside adapter
            TextView tvFirstLine = (TextView) rowView.findViewById(R.id.tvFirstLine);
            TextView tvSecondLine = (TextView) rowView.findViewById(R.id.tvSecondLine);
            ImageView ivIcon = (ImageView) rowView.findViewById(R.id.ivIcon);

            Trip trip = list.get(position);

            tvFirstLine.setText(trip.getTripLine());
            String tmpStr = Trip.simpleDateFormat.format(trip.getDate());
            tvSecondLine.setText(tmpStr);//todo: format date;

            ivIcon.setImageResource(android.R.drawable.star_on);
            return rowView;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadTasksFromFile();
        tripAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveTasktoFile();
    }


    //savefile

    private void saveTasktoFile() {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = openFileOutput(FileName, Context.MODE_PRIVATE);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
            for (Trip trip : tripList) {
                String todoStr = trip.printLine();
                bufferedWriter.write(todoStr);
//                bufferedWriter.write("\n");
            }
            bufferedWriter.close();
            outputStreamWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void loadTasksFromFile() {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = openFileInput(FileName);
            InputStreamReader bufferedInputStream = new InputStreamReader(fileInputStream);
            BufferedReader bf = new BufferedReader(bufferedInputStream);
            String line;
            tripList.clear();
            while ((line = bf.readLine()) != null) {
                String[] splist = line.split(",");

                String[] dateSplit = splist[2].split("-");
                Date date = new Date(Integer.parseInt(dateSplit[0]), Integer.parseInt(dateSplit[1]), Integer.parseInt(dateSplit[2]));

                Trip trip = new Trip(splist[0], splist[1], date, splist[3], Integer.parseInt(splist[4]));
                tripList.add(trip);
            }
            bf.close();
            bufferedInputStream.close();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "loadTasksFromFile: there is no file!");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //loadfile
}
