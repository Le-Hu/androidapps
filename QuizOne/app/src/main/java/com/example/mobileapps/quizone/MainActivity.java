package com.example.mobileapps.quizone;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static final String Extra_Message = "MainActivity.quiz";
    EditText etName;
    RadioGroup rgToast;
    CheckBox cbToasted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etName = (EditText) findViewById(R.id.etName);
        rgToast = (RadioGroup) findViewById(R.id.rgToast);
        cbToasted = (CheckBox) findViewById(R.id.cbToasted);
    }

    private String getOrderInfo() {
        String name = etName.getText().toString();
        String isChecked = "Toasted";
        if (!cbToasted.isChecked())
            isChecked = "Not Toasted";

        RadioButton rbCurrentChecked = (RadioButton) findViewById(rgToast.getCheckedRadioButtonId());


        String rbToastStr = rbCurrentChecked.getText().toString();
        return name + " make order here and " + rbToastStr + " " + isChecked;
    }

    public void onOrderHere(View view) {
        String summary = getOrderInfo();
        Toast.makeText(this, summary, Toast.LENGTH_LONG).show();
    }

    public void onOrderToGo(View view) {
        Intent intent = new Intent(this, ResultActivity.class);
        String summary = this.getOrderInfo();
        System.out.println(summary);
        intent.putExtra(Extra_Message, summary);
        startActivity(intent);
    }

}
